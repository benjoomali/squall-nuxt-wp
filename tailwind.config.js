module.exports = {
    mode: 'jit',
    theme: {
        screens: {
        'sm': '414px',
        // => @media (min-width: 414px) { ... } -difference between iPhone 5, 6, 8 and iPhone Plus
    
        'md': '600px',
        // => @media (min-width: 600px) { ... }
    
        'lg': '1024px',
        // => @media (min-width: 1024px) { ... }
    
        'xl': '1280px',
        // => @media (min-width: 1280px) { ... }
    
        '2xl': '1536px',
        // => @media (min-width: 1536px) { ... }

        '3xl': '1920px',
        // => @media (min-width: 1920px) { ... }

        '4xl': '2560px',
        // => @media (min-width: 2560px) { ... }

        '4k': '3840px',
        // => @media (min-width: 3840px) { ... }
        },
    },
    variants: {},
    plugins: [
      require('@tailwindcss/typography'),
    ],
    purge: {
      content: [
        'components/**/*.vue',
        'layouts/**/*.vue',
        'pages/**/*.vue',
        'plugins/**/*.js',
        'nuxt.config.js',
        // TypeScript
        'plugins/**/*.ts',
        'nuxt.config.ts'
      ]
    }
  }
  
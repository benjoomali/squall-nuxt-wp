export const state = () => ({
  posts: [],
  pages: [],
})

export const getters = {}

export const mutations = {
  SET_POSTS: (state, posts) => {
    state.posts = posts
  },
  SET_PAGES: (state, pages) => {
    state.pages = pages
  },
}

export const actions = {
  async getPosts({ state, commit }) {
    if (state.posts.length) return
    try {
      let posts = await this.$axios.$get(
        `/wp-json/wp/v2/posts?page=1&per_page=100&_embed=1`
      )
      // filter out unnecessary data
      posts = posts.map(({ id, slug, title, content }) => ({
        id,
        slug,
        title,
        content,
      }))
      commit('SET_POSTS', posts)
    } catch (err) {
      console.error('getPosts', err)
    }
  },
  async getPages({ state, commit }) {
    if (state.pages.length) return
    try {
      let pages = await this.$axios.$get(
        `/wp-json/wp/v2/pages?page=1&per_page=100`
      )
      // filter out unnecessary data
      pages = pages.map(({ id, slug, title, content, ACF }) => ({
        id,
        slug,
        title,
        content,
        ACF,
      }))
      commit('SET_PAGES', pages)
    } catch (err) {
      console.error('getPages', err)
    }
  },
}

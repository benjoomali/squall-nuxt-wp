# squall-nuxt-wp

This is a Jumpstart Template for Headless Wordpress built on Nuxt, TailwindCSS that populates data from Wordpress.

Copy the wp-content/themes/squall-nuxt-wp over to your Wordpress instance and activate the theme.

If you want to use the ACF configuration (Simple Header component) then please import the acf-export.json into your ACF plugin.

Change the baseURL in nuxt.config.js to your desired URL.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
